This is a simple module to provide a time field. This should be covered with the Date UI, however at this time there are 
validation issues with restricting the datetime field to time only. This provides a way to enter in only time. There is 
not much functionality with this module. It is meant only for developers to add in to their forms via FAPI. 

To install, copy this folder to your modules directory and enable.
